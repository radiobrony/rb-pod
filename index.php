<?php
// Composer
require __DIR__ . '/vendor/autoload.php';

// Fat Free Framework
$f3 = \Base::instance();

// The config
$f3->config(__DIR__ . '/app/config.ini');

// The DB
$dbConf = $f3->get('dbconf');
$db = new DB\SQL(
  'mysql:host=' . $dbConf['host'] .
  ';port=' . $dbConf['port'] .
  ';dbname=' . $dbConf['name'],
  $dbConf['user'],
  $dbConf['pass'],
  [\PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES utf8mb4;']
);
$f3->set('db', $db);

// Signing images
$iConf = $f3->get('imagor');
function imgsign($path) {
  global $iConf;
  $hash = hash_hmac('sha1', $path, $iConf['secret'], true);
  $bHash = str_replace('+', '-', str_replace('/', '_', base64_encode($hash)));
  return $iConf['base'] . $bHash . '/' . $path;
}
function coverSizes($f3, $url) {
  // Check if we have it cached
  $cached = $f3->get('covers["' . $url . '"]');
  if ($cached) return $cached;

  $sizes = array(
    // Small cover
    '90' => imgsign('90x90/' . $url),
    '180' => imgsign('180x180/' . $url),
    '270' => imgsign('270x270/' . $url),

    // Big cover
    '150' => imgsign('150x150/' . $url),
    '300' => imgsign('300x300/' . $url),
    '450' => imgsign('450x450/' . $url),

    // Player
    '96' => imgsign('96x96/filters:format(jpeg)/' . $url),
    '128' => imgsign('128x128/filters:format(jpeg)/' . $url),
    '192' => imgsign('192x192/filters:format(jpeg)/' . $url),
    '256' => imgsign('256x256/filters:format(jpeg)/' . $url),
    '384' => imgsign('384x384/filters:format(jpeg)/' . $url),
    '512' => imgsign('512x512/filters:format(jpeg)/' . $url),

    // Banner
    '970' => imgsign('970x0/' . $url),

    // iTunes cover
    '3000' => imgsign('3000x3000/filters:format(jpeg)/' . $url)
  );

  $f3->set('covers["' . $url . '"]', $sizes);
  return $sizes;
}

// And some default stuff
$f3->set('SBASE', 'https://pod.radiobrony.fr/'); // HTTPS + leading slash needed
$f3->set('XFRAME', "");

// Here we go
$f3->run();
