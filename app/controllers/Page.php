<?php
use daandesmedt\PHPHeadlessChrome\HeadlessChrome;

class Page {
  public function error($f3) {
    $p = substr($f3->get('PATH'), 0, 4);

    if ($p == '/api') {
      $output = array('error' => array(
        'code' => $f3->get('ERROR.code'),
        'message' => $f3->get('ERROR.text')
      ));

      header('Content-Type: application/json');
      echo json_encode($output, JSON_PRETTY_PRINT);
    } else if ($p == '/fee') {
      echo $f3->get('ERROR.text');
    } else {
      echo \Template::instance()->render('error.html');
    }
  }

  public function home($f3) {
    echo \Template::instance()->render('home.html');
  }

  public function player($f3) {
    $db = $f3->get('db');
    $db_s = new DB\SQL\Mapper($db, 'pod_shows');
    $db_e = new DB\SQL\Mapper($db, 'pod_episodes');

    $db_e->load(
      array('id = ?', $f3->get('PARAMS.id'))
    );

    if ($db_e->dry()) {
      // Doesn't exists
      $f3->error(404, 'Episode introuvable');
      exit;
    }

    $episode = $db_e->cast();

    $db_s->load(
      array('slug = ?', $episode['showSlug'])
    );

    $show = $db_s->cast();

    $episode['show'] = $show['title'];
    $episode['file'] = $f3->get('SBASE') . 'files/' . $show['baseFolder'] . $episode['file'];
    $episode['url'] = 'https://radiobrony.fr/podcasts/show/' . $episode['showSlug'] . '/episode/' . $episode['season'] . '/' . $episode['episode'];

    if ($episode['image']) {
      $episode['image'] = $f3->get('SBASE') . 'img/covers/' . $episode['image'];
    } else {
      $episode['image'] = $f3->get('SBASE') . 'img/shows/' . $show['slug'] . '.png';
    }

    $f3->set('ep', $episode);

    echo \Template::instance()->render('player.html');
  }

  public function ogShow($f3) {
    if (!ctype_alpha($f3->get('PARAMS.slug'))) die('SLUG invalide');

    $filePath = __DIR__ . '/../../tmp/cache/';
    $fileName =  $f3->get('PARAMS.slug') . '.jpg';

    if (file_exists($filePath . $fileName)) {
      header('Content-Type: image/jpeg');
      echo file_get_contents($filePath . $fileName);
    } else {
      $db = $f3->get('db');
      $db_s = new DB\SQL\Mapper($db, 'pod_shows');

      $db_s->load(
        array('slug = ?', $f3->get('PARAMS.slug'))
      );

      if ($db_s->dry()) {
        // Doesn't exists
        $f3->error(404, 'Show introuvable');
        exit;
      }

      $show = $db_s->cast();
      $show['image'] = $f3->get('SBASE') . 'img/shows/' . $show['slug'] . '.png';

      $f3->set('show', $show);

      $html = \Template::instance()->render('og-show.html');

      $headlessChromer = new HeadlessChrome();
      $headlessChromer->setBinaryPath('/usr/bin/google-chrome');
      $headlessChromer->setWindowSize(1200, 630);
      $headlessChromer->setOutputDirectory($filePath);
      $headlessChromer->setHTML($html);
      $headlessChromer->toScreenShot($fileName);

      header('Content-Type: image/jpeg');
      echo file_get_contents($headlessChromer->getFilePath());
    }
  }

  public function ogEpisode($f3) {
    if (!is_numeric($f3->get('PARAMS.id'))) die('ID invalide');

    $filePath = __DIR__ . '/../../tmp/cache/';
    $fileName =  $f3->get('PARAMS.id') . '.jpg';

    if (file_exists($filePath . $fileName)) {
      header('Content-Type: image/jpeg');
      echo file_get_contents($filePath . $fileName);
    } else {
      $db = $f3->get('db');
      $db_s = new DB\SQL\Mapper($db, 'pod_shows');
      $db_e = new DB\SQL\Mapper($db, 'pod_episodes');

      $db_e->load(
        array('id = ?', $f3->get('PARAMS.id'))
      );

      if ($db_e->dry()) {
        // Doesn't exists
        $f3->error(404, 'Episode introuvable');
        exit;
      }

      $episode = $db_e->cast();

      $db_s->load(
        array('slug = ?', $episode['showSlug'])
      );

      $show = $db_s->cast();

      $episode['show'] = $show['title'];

      if ($episode['image']) {
        $episode['image'] = $f3->get('SBASE') . 'img/covers/' . $episode['image'];
      } else {
        $episode['image'] = $f3->get('SBASE') . 'img/shows/' . $show['slug'] . '.png';
      }

      $f3->set('ep', $episode);

      $html = \Template::instance()->render('og-episode.html');

      $headlessChromer = new HeadlessChrome();
      $headlessChromer->setBinaryPath('/usr/bin/google-chrome');
      $headlessChromer->setWindowSize(1200, 630);
      $headlessChromer->setOutputDirectory($filePath);
      $headlessChromer->setHTML($html);
      $headlessChromer->toScreenShot($fileName);

      header('Content-Type: image/jpeg');
      echo file_get_contents($headlessChromer->getFilePath());
    }
  }
}
