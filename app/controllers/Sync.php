<?php
class Sync {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('shows', new DB\SQL\Mapper($db, 'pod_shows'));
    $f3->set('episodes', new DB\SQL\Mapper($db, 'pod_episodes'));
  }

  public function syncFiles($f3) {
    // Prints out possible missing entries
    $s = $f3->get('shows');
    $e = $f3->get('episodes');

    echo '<h1>SyncFiles</h1>';

    $s->load(
      array(
        'slug = ?',
        $f3->get('PARAMS.slug')
      )
    );

    echo '<h2>' . $s->slug . '</h2>';

    $eps = [];
    $base = '/var/lib/mpd/music/';
    $files = array_diff(scandir($base . $s->baseFolder), array('..', '.'));

    // load up files we already have
    $e->load(
      array(
        'showSlug = ?',
        $s->slug
      )
    );

    while (!$e->dry()) {
      $eps[] = urldecode($e->file);
      $e->next();
    }

    $missing = array_diff($files, $eps);

    echo '<code><pre>';

    foreach ($missing as $miss) {
      $file = $base . $s->baseFolder . $miss;
      $size = filesize($file);

      $date = new DateTime('@' . filemtime($file));
      $date->setTime(21, 00);
      $fileDate = $date->format('Y-m-d H:i:s');

      $getID3 = new getID3;
      $tag = $getID3->analyze($file);
      $duration = ceil($tag['playtime_seconds']);

      echo 'INSERT INTO `pod_episodes` (`showSlug`, `title`, `file`, `size`, `pubDate`, `episode`, `duration`, `block`) ';
      echo 'VALUES (\'' . $s->slug . '\', \'' . str_replace('.mp3', '', $miss) . '\', \'' . rawurlencode($miss) . '\', ' . $size . ', ';
      echo '\'' . $fileDate . '\', 0, ' . $duration . ', 1);<br/>';
    }

    echo '</pre></code>';

  }

  public function syncMeta($f3) {
    $base = '/var/lib/mpd/music/';
    $reChapters = '/([0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}) (.+?)($| <(.*)>$)/m';

    $f3->get('episodes')->load(
      array(
        'size IS NULL OR duration IS NULL'
      )
    );

    while(!$f3->get('episodes')->dry()) {
      $f3->get('shows')->load(
        array(
          'slug = ?',
          $f3->get('episodes')->showSlug
        )
      );

      $file = $base . $f3->get('shows')->baseFolder . urldecode($f3->get('episodes')->file);
      $fileChapters = str_replace('.mp3', '.chapters.txt', $file);

      if (!file_exists($file)) {
        echo '<br/>**NOT FOUND: ' . $file . '**';
        $f3->get('episodes')->next();
        continue;
      }

      if (!file_exists($fileChapters)) {
        $f3->get('episodes')->chapters = '[]';
      } else {
        $chapters = [];
        $fc = file_get_contents($fileChapters);
        preg_match_all($reChapters, $fc, $matches, PREG_SET_ORDER, 0);

        foreach($matches as $match) {
          $url = null;
          if (array_key_exists(4, $match)) $url = $match[4];

          $dtp = date_parse($match[1]);
          $ts = $dtp['hour'] * 3600 + $dtp['minute'] * 60 + $dtp['second'];

          $chapters[] = array(
            'ts' => $ts,
            'start' => $match[1],
            'title' => $match[2],
            'href' => $url
          );
        }

        $f3->get('episodes')->chapters = json_encode($chapters, JSON_UNESCAPED_SLASHES);
      }

      $size = filesize($file);
      $getID3 = new getID3;
      $tag = $getID3->analyze($file);
      $duration = ceil($tag['playtime_seconds']);

      $f3->get('episodes')->size = $size;
      $f3->get('episodes')->duration = $duration;
      echo '<br/>' . $file . ' => Size: ' . $size . ' - Duration: ' . $duration;
      $f3->get('episodes')->save();
      $f3->get('episodes')->next();
    }
  }

  public function syncPubSub($f3) {
    // This function is intended to be called by a cron
    // to push new feed updates to Google's PubSubHub

    // We're going to list every episodes past their publish date
    // who did not get their announce bit set to 1 and push
    // the show's slug to an array
    $updated = [];
    $f3->get('episodes')->load(
      array(
        'pubsubbed=0 AND pubDate <= NOW()'
      )
    );
    while(!$f3->get('episodes')->dry()) {
      $s = $f3->get('BASE_FEED_URL') . $f3->get('episodes')->showSlug;
      if (!in_array($s, $updated)) $updated[] = $s;

      $f3->get('episodes')->pubsubbed = 1;
      $f3->get('episodes')->save();
      $f3->get('episodes')->next();
    }

    // If we've got updates, let's make a POST to notify the hub
    if (count($updated) > 0) {
      $updated[] = $f3->get('BASE_FEED_URL') . 'all';

      $data = 'hub.mode=publish';
      foreach ($updated as $url) {
        $data .= '&hub.url=' . $url;
      }

      $ch = curl_init('https://pubsubhubbub.appspot.com/');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
      $result = curl_exec($ch);
      curl_close($ch);

      echo $result;
    }
  }

  public function syncActivitypub($f3) {
    $ch = curl_init($f3->get('MASTODON_FEED'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    $feed = json_decode($result, true);

    foreach($feed as $s) {
      if (array_key_exists('card', $s) && $s['card']) {
        $re = '/podcasts\/show\/([a-z]+)\/episode\/([0-9\.\-]+)\/([0-9\.\-]+)/';

        preg_match_all($re, $s['card']['url'], $matches, PREG_SET_ORDER, 0);

        $f3->get('episodes')->load(
          array(
            'showSlug = ? AND season = ? AND episode = ? AND activitypub IS NULL',
            $matches[0][1],
            $matches[0][2],
            $matches[0][3]
          )
        );

        if (!$f3->get('episodes')->dry()) {
          $f3->get('episodes')->activitypub = $s['uri'];
          $f3->get('episodes')->save();
        }
      }
    }
  }

  public function syncChapters($f3) {
    $base = '/var/lib/mpd/music/';
    $re = '/([0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}) (.+?)($| <(.*)>$)/m';

    $f3->get('episodes')->load(
      array(
        'chapters IS NULL'
      )
    );

    while(!$f3->get('episodes')->dry()) {
      $f3->get('shows')->load(
        array(
          'slug = ?',
          $f3->get('episodes')->showSlug
        )
      );

      $file = $base . $f3->get('shows')->baseFolder . urldecode($f3->get('episodes')->file);
      $fileTXT = str_replace('.mp3', '.chapters.txt', $file);

      if (!file_exists($fileTXT)) {
        $f3->get('episodes')->chapters = '[]';
        $f3->get('episodes')->save();
        $f3->get('episodes')->next();
        continue;
      }

      $chapters = [];
      $fc = file_get_contents($fileTXT);
      preg_match_all($re, $fc, $matches, PREG_SET_ORDER, 0);

      foreach($matches as $match) {
        $url = null;
        if (array_key_exists(4, $match)) $url = $match[4];

        $dtp = date_parse($match[1]);
        $ts = $dtp['hour'] * 3600 + $dtp['minute'] * 60 + $dtp['second'];

        $chapters[] = array(
          'ts' => $ts,
          'start' => $match[1],
          'title' => $match[2],
          'href' => $url
        );
      }

      $f3->get('episodes')->chapters = json_encode($chapters, JSON_UNESCAPED_SLASHES);
      echo '<br/>' . $fileTXT . ' found';
      $f3->get('episodes')->save();
      $f3->get('episodes')->next();
    }
  }
}
