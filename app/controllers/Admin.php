<?php
class Admin {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('users', new DB\SQL\Mapper($db, 'pod_users'));
    $f3->set('episodes', new DB\SQL\Mapper($db, 'pod_episodes'));
  }

  public function login($f3) {
    $f3->get('users')->load(
      array(
        'userName = ? AND userPass = ?',
        $f3->get('POST.user'),
        $f3->get('POST.pass')
      )
    );

    if ($f3->get('users')->dry()) {
      // Doesn't exists
      $f3->error(403, 'Identifiants invalides.');
      exit;
    }

    $token = bin2hex(openssl_random_pseudo_bytes(64));

    $f3->get('users')->userToken = $token;
    $f3->get('users')->save();

    echo json_encode(array(
      'user' => $f3->get('POST.user'),
      'token' => $token,
    ));
  }

  public function check($f3) {
    $f3->get('users')->load(
      array(
        'userName = ? AND userToken = ?',
        $f3->get('POST.user'),
        $f3->get('POST.token')
      )
    );

    if ($f3->get('users')->dry()) {
      $f3->error(403, 'Invalid token.');
      exit;
    }

    echo json_encode(array('results' => 'pass'));
  }

  public function listFiles($f3) {
  }

  public function add($f3) {
    echo json_encode(array('results' => 'pass'));
  }

  public function edit($f3) {
    echo json_encode(array('results' => 'pass'));
  }

  public function delete($f3) {
    echo json_encode(array('results' => 'pass'));
  }
}
