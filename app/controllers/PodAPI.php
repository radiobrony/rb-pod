<?php
class PodAPI {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('shows', new DB\SQL\Mapper($db, 'pod_shows'));
    $f3->set('episodes', new DB\SQL\Mapper($db, 'pod_episodes'));
  }

  private function cleanShow($show, $f3, $details=false, $rss=false) {
    // TINYINT to Boolean
    $show['explicit'] = (bool) $show['explicit'];

    // Set image URL
    $show['images'] = [];
    $show['images']['banner'] = $f3->get('SBASE') . 'img/shows/' . $show['slug'] . '-ban.png';
    //$show['images']['cover'] = $f3->get('SBASE') . 'img/shows/' . $show['slug'] . ($rss ? '-res.png' : '.png');
    $show['images']['cover'] = $f3->get('SBASE') . 'img/shows/' . $show['slug'] . '.png';

    // Covers signed for imagor
    $show['images'] += coverSizes($f3, $f3->get('SBASE') . 'img/shows/' . $show['slug'] . '.png');

    // Fix empty categories
    if (!$show['category']) {
      $show['category'] = 'Games &amp; Hobbies';
      $show['subCategory'] = 'Hobbies';
    }

    // Add fields
    $show['feed'] = $f3->get('SBASE') . 'feed/' . $show['slug'];

    // If we're in detail view or not
    if (!$details) {
      unset($show['description']);
      $show['_episodes'] = $f3->get('SBASE') . 'api/show/' . $show['slug'];
    }

    // Remove internal stuff
    if ($f3->get('GET.int') != 'true') {
      unset($show['id']);
      unset($show['order']);
      unset($show['tracking']);
      unset($show['donation']);
      unset($show['baseFolder']);
    }

    return $show;
  }

  private function cleanEpisode($episode, $show, $f3, $details=false, $rss=false, $nuxt=false) {
    // Return 404 if it doesn't have some metas or if it's a future episode
    if (!$episode['size'] || !$episode['duration'] || strtotime($episode['pubDate']) > time()) {
      if ($rss || !$details) return;
      $f3->error(404, 'Episode not found');
      exit;
    }

    // Check if we ask for internal stuff
    $internals = $f3->get('GET.int') == 'true';

    // TINYINT to Boolean
    $episode['block'] = (bool) $episode['block'];
    $episode['spoiler'] = (bool) $episode['spoiler'];
    $episode['explicit'] = (bool) $episode['explicit'];

    // TEXT (JSON) to Object
    $episode['chapters'] = json_decode($episode['chapters'], true);

    // Fix URLs
    if (!$internals) {
      $episode['file'] = $f3->get('SBASE') . 'files/' . $show['baseFolder'] . $episode['file'];
      if ($episode['image']) {
        $episode['image'] = $f3->get('SBASE') . 'img/covers/' . $episode['image'];
      } else {
        //$episode['image'] = $f3->get('SBASE') . 'img/shows/' . $show['slug'] . ($rss ? '-res.png' : '.png');
        $episode['image'] = $f3->get('SBASE') . 'img/shows/' . $show['slug'] . '.png';
      }

      // Covers signed for imagor
      $episode['cover'] = coverSizes($f3, $episode['image']);
    }

    // Add podcast page link
    $episode['link'] = 'https://radiobrony.fr/podcasts/show/' . $show['slug'] . '/episode/' . $episode['season'] . '/' . $episode['episode'];

    // Convert YouTube iframes to links OR iframes/links to lite-youtube OR links to YouTube iframes
    if ($rss) {
      // Link we want
      $rep = '<a target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/watch?v=${1}">https://www.youtube.com/watch?v=${1}</a>';
      // First with content who has videoWrapper
      $re = '/<div class="videoWrapper">.*youtube.com\/embed\/([0-9a-zA-Z_-]+).*<\/div>/';
      $episode['description'] = preg_replace($re, $rep, $episode['description']);
      // Next the lite-youtube embeds
      $re = '/<lite-youtube .*videoid="([0-9a-zA-Z_-]+)".*<\/lite-youtube>/';
      $episode['description'] = preg_replace($re, $rep, $episode['description']);
      // Then, for the rest
      $re = '/<iframe.*youtube.com\/embed\/([0-9a-zA-Z_-]+).*<\/iframe>/';
      $episode['description'] = preg_replace($re, $rep, $episode['description']);
    } else if ($nuxt) {
      // Element we want
      $rep = '<lite-youtube videoid="${1}" params="hl=fr&rel=0"><a class="lite-youtube-fallback" target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/watch?v=${1}">https://www.youtube.com/watch?v=${1}</a></lite-youtube>';
      // First with content who has videoWrapper
      $re = '/<div class="videoWrapper">.*youtube.com\/embed\/([0-9a-zA-Z_-]+).*<\/div>/';
      $episode['description'] = preg_replace($re, $rep, $episode['description']);
      // Then, iframes
      $re = '/<iframe.*youtube.com\/embed\/([0-9a-zA-Z_-]+).*<\/iframe>/';
      $episode['description'] = preg_replace($re, $rep, $episode['description']);
      // And finally simple links
      // -- youtube.com
      $re = '/^[<br \/>]*https?:\/\/www\.youtube\.com\/watch\?v=([0-9a-zA-Z_-]+)/m';
      $episode['description'] = preg_replace($re, $rep, $episode['description']);
      // -- youtu.be
      $re = '/^[<br \/>]*https?:\/\/youtu.be\/([0-9a-zA-Z_-]+)/m';
      $episode['description'] = preg_replace($re, $rep, $episode['description']);
    } else {
      // Iframe we want
      $rep = '<div class="videoWrapper"><iframe loading="lazy" width="480" height="270" src="https://www.youtube.com/embed/${1}" frameborder="0" allowfullscreen></iframe></div>';
      // -- youtube.com
      $re = '/^[<br \/>]*https?:\/\/www\.youtube\.com\/watch\?v=([0-9a-zA-Z_-]+)/m';
      $episode['description'] = preg_replace($re, $rep, $episode['description']);
      // -- youtu.be
      $re = '/^[<br \/>]*https?:\/\/youtu.be\/([0-9a-zA-Z_-]+)/m';
      $episode['description'] = preg_replace($re, $rep, $episode['description']);
    }

    // If tracking is enabled and not bypassed
    if ($show['tracking'] && !$internals && $f3->get('GET.direct') != 'true') {
      $episode['file'] = str_replace('https://', 'https://dts.podtrac.com/redirect.mp3/', $episode['file']);
    }

    // Fix empty values
    if (!$episode['author']) $episode['author'] = $show['author'];
    if (!$episode['duration']) $episode['duration'] = 0;
    if (!$episode['size']) $episode['size'] = 0;

    // If we're in detail view or not
    if (!$details) {
      unset($episode['description']);
      unset($episode['file']);
      unset($episode['size']);
      $episode['_episode'] = $f3->get('SBASE') . 'api/show/' . $show['slug'] . '/episode/' . $episode['season'] . '/' . $episode['episode'];
    } else {
      // add nl2br in some cases
      if (
        $show['slug'] != 'weeklymix' && $show['slug'] != 'weeklymix-mixtape' // if it's not a weeklymix and a weeklymix-mixtape
        && $show['slug'] != 'ponyclub-hs' && $show['slug'] != 'ponyclub-mini' // if it's not a Pony Club HS/mini
        && !($show['slug'] == 'hebdopony' && strpos($episode['description'], '<li>') !== false ) // if it not an hebdopony with <li>s
        && strpos($episode['description'], '<br') === false // if there isn't any <br/>s already
        && strpos($episode['description'], '<p') === false // if there isn't any <p>s already
        && str_replace(array('-', ' ', ':'), '', $episode['pubDate']) < 20180300000000 // and finally if it's an old podcast (before march 2018)
      ) {
        $episode['description'] = nl2br($episode['description']);
      }

      // If there's no <p>, put everything in one
      if (strpos($episode['description'], '<p') === false) {
        $episode['description'] = '<p class="old-content">' . $episode['description'] . '</p>';
      }
    }

    // Make all of description stay on one line for RSS
    if ($rss) {
      // Remove new lines
      $episode['description'] = preg_replace('/\s+/', ' ', $episode['description']);
      // Remove multiple spaces
      $episode['description'] = trim(preg_replace('!\s+!', ' ', $episode['description']));
    }

    // Remove internal stuff
    if (!$internals) {
      unset($episode['showSlug']);
    }

    return $episode;
  }

  public function getLatest($f3) {
    // Return the last 15 episodes, ordered by pub date
    $latest = [];

    $f3->get('episodes')->load(
      array(
        'pubDate <= NOW()'
      ),
      array(
        'order' => 'pubDate DESC',
        'limit' => '15'
      )
    );

    while(!$f3->get('episodes')->dry()) {
      $f3->get('shows')->load(
        array(
          'slug = ?', $f3->get('episodes')->showSlug
        )
      );

      $ep = array(
        'show' => $this->cleanShow($f3->get('shows')->cast(), $f3, true),
        'episode' => $this->cleanEpisode($f3->get('episodes')->cast(), $f3->get('shows')->cast(), $f3, true, true) // Yes, return it like an RSS
      );

      if ($ep) $latest[] = $ep;
      $f3->get('episodes')->next();
    }

    $output = array(
      '_meta' => array(
        'request' => $f3->get('REALM'),
        'timestamp' => time()
      ),
      'latest' => $latest
    );

    header('Content-Type: application/json');
    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function getList($f3) {
    // Get a list of shows
    $shows = [];

    $f3->get('shows')->load(
      array(),
      array(
        'order' => 'order ASC'
      )
    );

    while(!$f3->get('shows')->dry()) {
      $shows[] = $this->cleanShow($f3->get('shows')->cast(), $f3);
      $f3->get('shows')->next();
    }

    $output = array(
      '_meta' => array(
        'request' => $f3->get('REALM'),
        'timestamp' => time()
      ),
      'shows' => $shows
    );

    header('Content-Type: application/json');
    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function getShow($f3) {
    // Support pagination
    $page = (int) $f3->get('GET.page');
    $limit = 500;
    $offset = 0;
    // Make sure it's a number and positive
    if (!is_numeric($page) || $page < 1) $page = 1;
    // TODO: remove if in 2023
    if ($f3->get('GET.page')) {
      $limit = 15;
      $offset = ($page - 1) * $limit;
    }

    // Get show and its episodes
    $f3->get('shows')->load(
      array(
        'slug = ?', $f3->get('PARAMS.show')
      )
    );

    if ($f3->get('shows')->dry()) {
      // Doesn't exists
      $f3->error(404, 'Show not found');
      exit;
    }

    $episodes = [];

    $epCount = $f3->get('db')->exec(
      'SELECT COUNT(id) AS cnt FROM pod_episodes WHERE showSlug=?',
      $f3->get('PARAMS.show')
    )[0]['cnt'];
    $maxPage = ceil($epCount / $limit);

    $f3->get('episodes')->load(
      array(
        'showSlug = ?', $f3->get('PARAMS.show')
      ),
      array(
        'order' => 'pubDate DESC',
        'offset' => $offset,
        'limit' => $limit
      )
    );

    while(!$f3->get('episodes')->dry()) {
      $ep = $this->cleanEpisode($f3->get('episodes')->cast(), $f3->get('shows')->cast(), $f3);
      if ($ep) $episodes[] = $ep;
      $f3->get('episodes')->next();
    }

    $output = array(
      '_meta' => array(
        'request' => $f3->get('REALM'),
        'timestamp' => time(),
        'page' => $page,
        'limit' => $limit,
        'maxPage' => $maxPage
      ),
      'show' => $this->cleanShow($f3->get('shows')->cast(), $f3, true),
      'episodes' => $episodes
    );

    header('Content-Type: application/json');
    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function getEpisode($f3) {
    // Get episode
    $f3->get('shows')->load(
      array(
        'slug = ?', $f3->get('PARAMS.show')
      )
    );

    if ($f3->get('shows')->dry()) {
      // Doesn't exists
      $f3->error(404, 'Show not found');
      exit;
    }

    $f3->get('episodes')->load(
      array(
        'showSlug = ? AND season = ? AND episode = ?',
        $f3->get('PARAMS.show'),
        $f3->get('PARAMS.se'),
        $f3->get('PARAMS.ep')
      )
    );

    if ($f3->get('episodes')->dry()) {
      // Doesn't exists
      $f3->error(404, 'Episode not found');
      exit;
    }

    $nuxt = false;
    $lowerHeaders = array_change_key_case($f3->get('HEADERS'), CASE_LOWER);
    if (array_key_exists('x-fromnuxt', $lowerHeaders)) $nuxt = true;

    $output = array(
      '_meta' => array(
        'request' => $f3->get('REALM'),
        'timestamp' => time()
      ),
      'show' => $this->cleanShow($f3->get('shows')->cast(), $f3, true),
      'episode' => $this->cleanEpisode($f3->get('episodes')->cast(), $f3->get('shows')->cast(), $f3, true, false, $nuxt)
    );

    header('Content-Type: application/json');
    echo json_encode($output, JSON_PRETTY_PRINT);
  }

  public function getEpisodeChapters($f3) {
    // Get episode
    $f3->get('shows')->load(
      array(
        'slug = ?', $f3->get('PARAMS.show')
      )
    );

    if ($f3->get('shows')->dry()) {
      // Doesn't exists
      $f3->error(404, 'Show not found');
      exit;
    }

    $f3->get('episodes')->load(
      array(
        'showSlug = ? AND season = ? AND episode = ?',
        $f3->get('PARAMS.show'),
        $f3->get('PARAMS.se'),
        $f3->get('PARAMS.ep')
      )
    );

    if ($f3->get('episodes')->dry()) {
      // Doesn't exists
      $f3->error(404, 'Episode not found');
      exit;
    }

    $isFirst = true;
    $chapters = [];

    $chs = json_decode($f3->get('episodes')->chapters, true);
    foreach($chs as $ch) {
      // Add "intro" entry if the first chapter isn't at second 0 or 1
      if ($isFirst) {
        $isFirst = false;
        if ($ch['ts'] > 1) {
          $chapters[] = array(
            'startTime' => 0,
            'title' => 'Intro'
          );
        }
      }

      // Add "url" key only if there's an URL
      $url = array();
      if ($ch['href']) $url = array('url' => $ch['href']);

      $chapters[] = array_merge(
        array(
            'startTime' => $ch['ts'],
            'title' => $ch['title']
        ),
        $url
      );
    }

    $output = array(
      'version' => '1.2.0',
      'author' => $f3->get('episodes')->author,
      'title' => $f3->get('episodes')->title,
      'podcastName' => $f3->get('shows')->title,
      'description' => $f3->get('episodes')->subtitle,
      'fileName' =>  $f3->get('SBASE') . 'files/' . $f3->get('shows')->baseFolder . $f3->get('episodes')->file,
      'chapters' => $chapters
    );

    header('Content-Type: application/json+chapters');
    echo json_encode($output);
  }

  public function getXML($f3) {
    // Get show and its episodes
    $f3->get('shows')->load(
      array(
        'slug = ?', $f3->get('PARAMS.show')
      )
    );

    if ($f3->get('shows')->dry()) {
      // Doesn't exists
      $f3->error(404, 'Show not found');
      exit;
    }

    $episodes = [];
    $page = $f3->get('PARAMS.page') > 0 ? ($f3->get('PARAMS.page') - 1) : 0;

    $paged = $f3->get('episodes')->paginate(
      $page,
      15,
      array(
        'showSlug = ?', $f3->get('shows')->slug
      ),
      array(
        'order' => 'pubDate DESC'
      )
    );

    foreach ($paged['subset'] as $epRaw) {
      $ep = $this->cleanEpisode($epRaw->cast(), $f3->get('shows')->cast(), $f3, true, true);
      if ($ep) $episodes[] = $ep;
    }

    $uuid = Kodus\Helpers\UUIDv5::create(
      'ead4c236-bf58-58c6-a2c6-a6b28d128cb6',
      str_replace('https://', '', $f3->BASE_FEED_URL) . $f3->get('shows')->slug
    );

    $f3->set('d', array(
      'show' => $this->cleanShow($f3->get('shows')->cast(), $f3, true, true),
      'episodes' => $episodes,
      'self' => $f3->BASE_FEED_URL . $f3->get('shows')->slug,
      'pagination' => array(
        'curr' => $paged['pos'] + 1,
        'last' => intval($paged['count'], 10)
      ),
      'uuid' => $uuid
    ));

    echo \Template::instance()->render('feed.xml', 'application/xml');
  }

  public function getAllXML($f3) {
    // Get all episodes
    $episodes = [];
    $page = $f3->get('PARAMS.page') > 0 ? ($f3->get('PARAMS.page') - 1) : 0;

    $paged = $f3->get('episodes')->paginate(
      $page,
      15,
      null,
      array(
        'order' => 'pubDate DESC'
      )
    );

    foreach ($paged['subset'] as $epRaw) {
      $f3->get('shows')->load(
        array(
          'slug = ?', $epRaw->showSlug
        )
      );
      $ep = $this->cleanEpisode($epRaw->cast(), $f3->get('shows')->cast(), $f3, true, true);
      if ($ep) $episodes[] = $ep;
    }

    $uuid = Kodus\Helpers\UUIDv5::create(
      'ead4c236-bf58-58c6-a2c6-a6b28d128cb6',
      str_replace('https://', '', $f3->BASE_FEED_URL) . 'all'
    );

    $f3->set('d', array(
      'show' => array(
        'slug' => 'all',
        'ext' => null,
        'title' => 'Radio Brony - Toutes les émissions',
        'link' => 'https://radiobrony.fr',
        'author' => 'Radio Brony',
        'description' => 'Flux unique qui regroupe toutes les dernères émissions de Radio Brony',
        'subtitle' => null,
        'category' => 'Games &amp; Hobbies',
        'subCategory' => 'Hobbies',
        'explicit' => false,
        'itunes' => null,
        'lastUpdate' => null,
        'images' => coverSizes($f3, 'https://radiobrony.fr/img/icons/bwavatar-details.png'),
        'feed' => 'https://pod.radiobrony.fr/feed/all'
      ),
      'episodes' => $episodes,
      'self' => $f3->BASE_FEED_URL . 'all',
      'pagination' => array(
        'curr' => $paged['pos'] + 1,
        'last' => intval($paged['count'], 10)
      ),
      'uuid' => $uuid
    ));

    echo \Template::instance()->render('feed.xml', 'application/xml');
  }
}
