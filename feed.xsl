<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
  xmlns:podcast="https://podcastindex.org/namespace/1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html" indent="no"/>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />

    <xsl:choose>
      <xsl:when test="$text = '' or $replace = '' or not($replace)">
        <!-- Prevent this routine from hanging -->
        <xsl:value-of select="$text" />
      </xsl:when>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text, $replace)" />
        <xsl:value-of select="$by" />

        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text" select="substring-after($text, $replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/">
    <html lang="fr">
      <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="{rss/channel/description}" />
        <title><xsl:value-of select="rss/channel/title" /> | RSS | Radio Brony</title>
        <style>
          body {
            background-attachment: fixed;
            background-color: #a6d9f0;
            background-image: url("/img/day.bg.jpg");
            background-repeat: no-repeat;
            background-size: cover;
            font-family: "Helvetica Neue", "Helvetica", "Arial", sans-serif;
            margin: 0;
            overflow-y: scroll;
            padding: 15px;
          }

          b {
            font-weight: bold;
          }

          blockquote {
            clear: both;
          }

          hr {
            border: 0;
            height: 1px !important;
            background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
          }

          /* Balise spoiler de fortune */
          s {
            background-color: black;
            color: black;
            text-decoration: none !important;
          }
          s:hover {
            color: white;
          }

          .btn {
            -moz-appearance: none;
            -webkit-appearance: none;
            background: #444;
            border-radius: 4px;
            border: 1px solid #d3d6db;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            height: 16px;
            padding: 5px 8px;
            position: relative;
            text-align: center;
            text-decoration: none;
            user-select: none;
            vertical-align: top;
            white-space: nowrap;
          }

          nav {
            height: 27px;
            margin-top: 0.5em;
            text-align: center;
          }
          nav .btn-container {
            min-width: 100px;
          }
          nav .nav-prev {
            float: left;
            text-align: left;
          }
          nav .nav-next {
            float: right;
            text-align: right;
          }
          nav .subscribe {
            border-radius: 4px;
            height: 27px;
            margin: 0px auto;
            opacity: 0;
            overflow: hidden;
            transition: opacity .5s;
            width: 108px;
          }

          #feed {
            background-color: #ffffff;
            box-shadow: 0 0 10px 0 rgba(50, 50, 50, 0.75);
            color: #000000;
            display: flex;
            flex-direction: column;
            margin: 0 auto;
            max-width: 900px;
            min-height: calc(100vh - 80px);
            padding: 15px;
          }
          #feed > div {
            flex-grow: 1;
          }

          .main-header {
            background-color: #a6d9f0;
            background-image: url("/img/day.ban.jpg");
            background-position: top center;
            background-repeat: no-repeat;
            background-size: cover;
            color: #000000;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            margin-top: 15px;
            padding: 0 15px;
          }
          .main-header .logo {
            align-self: center;
            flex: 0;
            padding-right: 15px;
          }
          .main-header .logo .logo-el {
            display: flex;
            height: 140px;
            justify-content: center;
            margin-bottom: -3px;
            width: 248px;
          }
          .main-header .logo .logo-el img {
            align-self: center;
            max-height: 140px;
            position: absolute;
          }
          .main-header .title {
            align-content: center;
            align-items: flex-start;
            display: flex;
            flex: 1;
            flex-flow: column nowrap;
            justify-content: center;
            padding: 15px 0;
            overflow: hidden;
          }
          .main-header .title div {
            align-self: auto;
            background-color: rgba(255, 255, 255, 0.8);
            flex: 0 1 auto;
            font-size: 30px;
            max-width: 100%;
            overflow: hidden;
            padding: 2px 5px;
            text-overflow: ellipsis;
            white-space: nowrap;
          }
          .main-header .title .page {
            font-size: 30px;
            font-weight: bold;
            order: 1;
          }
          .main-header .title .url {
            font-size: 20px;
            order: 2;
          }

          .main-footer {
            color: #999999;
            font-size: 0.9rem;
            line-height: 1.3rem;
            margin: 30px 0 10px;
            text-align: center;
          }
          .main-footer a,
          .main-footer a:visited {
            color: inherit;
            cursor: pointer;
            text-decoration: underline;
          }

          .all-eps a {
            color: inherit;
            text-decoration: none;
          }

          .content {
            margin-top: 20px;
          }

          .episode {
            border: 1px solid gray;
            border-radius: 5px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
            display: flex;
            margin: 10px 0;
            min-height: 90px;
            overflow: hidden;
            transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
          }
          /*
          .episode:hover {
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
          }
          */
          .episode .cover {
            height: 90px;
            min-height: 90px;
            min-width: 90px;
            width: 90px;
          }
          .episode .text {
            flex-grow: 1;
            padding: 15px;
          }
          .episode .text h4 {
            margin: 0;
          }
          .episode .text blockquote {
            margin: 10px 0;
            border-left: 3px gray solid;
            padding: 5px 10px;
          }
          .episode audio {
            margin-top: 15px;
            width: 100%;
          }

          .blurred-banner .blurcontainer {
            height: 200px;
            overflow: hidden;
          }
          .blurred-banner .blurcontainer .blurbackground {
            background-position: center;
            background-size: 125%;
            filter: blur(50px);
            height: 400px;
            margin: -95px -35px;
            position: relative;
            z-index: 1;
          }
          .blurred-banner .details-content {
            align-content: center;
            align-items: center;
            display: flex;
            flex-flow: row nowrap;
            flex: 1;
            height: 200px;
            justify-content: center;
            margin: -200px 25px 0 25px;
            overflow: hidden;
          }
          .blurred-banner .details-content .cover {
            height: 150px;
            margin-right: 25px;
            min-height: 150px;
            min-width: 150px;
            width: 150px;
            z-index: 2;
          }
          .blurred-banner .details-content .details-text {
            align-content: center;
            align-items: flex-start;
            display: flex;
            flex-flow: column nowrap;
            flex: 1;
            height: 200px;
            justify-content: center;
            overflow: hidden;
          }
          .blurred-banner .details-content .details-text h3,
          .blurred-banner .details-content .details-text span {
            background-color: rgba(255, 255, 255, 0.8);
            display: inline-block;
            font-size: 1.1em;
            margin: 0;
            overflow: hidden;
            padding: 2px 5px;
            z-index: 2;
          }
          .blurred-banner .details-content .details-text h3 {
            font-size: 1.25em;
          }

          @media (max-width: 719px) {
            body {
              padding: 0;
            }

            #feed {
              min-height: calc(100vh - 30px);
            }

            .main-header {
              flex-direction: column;
              flex-wrap: nowrap;
              justify-content: center;
            }
            .main-header .logo {
              max-width: 100%;
              padding: 0;
              width: 248px;
            }
            .main-header .logo .logo-el {
              margin-bottom: -5px;
              max-width: 100%;
            }
            .main-header .logo .logo-el img {
              max-width: calc(100vw - 45px);
            }
            .main-header .title {
              flex: initial;
              padding-top: 0;
            }
            .main-header .title .url {
              display: none;
            }

            .blurred-banner .blurcontainer {
              display: none;
            }
            .blurred-banner .details-content {
              height: auto;
              margin: 0;
            }
            .blurred-banner .details-content .cover {
              display: none;
            }
            .blurred-banner .details-content .details-text {
              height: auto;
            }
            .blurred-banner .details-content .details-text h3,
            .blurred-banner .details-content .details-text span {
              background-color: transparent !important;
              padding: 2px 0;
            }
          }

          @media (prefers-color-scheme: dark) {
            body {
              background-image: url("/img/night.bg.jpg");
            }

            #feed {
              background-color: #292829;
              color: #ffffff;
            }

            .btn {
              border-color: #444444;
            }

            hr {
              background-image: linear-gradient(to right, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.75), rgba(255, 255, 255, 0));
            }

            s {
              background-color: white;
              color: white;
              text-decoration: none !important;
            }
            s:hover {
              color: black;
            }

            .main-header {
              background-image: url("/img/night.ban.jpg");
            }

            .episode {
              background-color: #333333;
            }

            .blurred-banner .details-content .details-text h3,
            .blurred-banner .details-content .details-text span {
              background-color: rgba(0, 0, 0, 0.7);
              color: #ffffff;
            }
          }
        </style>
        <script src="https://cdn.jsdelivr.net/npm/luxon@3/build/global/luxon.min.js"></script>
      </head>
      <body>
        <div id="feed">
          <header class="main-header">
            <div class="logo">
              <div class="logo-el">
                <img alt="Radio Brony" src="https://t.radiobrony.fr/haTYp79hNIMql976V1eGmp3cOlw=/0x140/https://radiobrony.fr/img/logos/RBLogo.png" />
              </div>
            </div>
            <div class="title">
              <div class="page">
                Radio Brony Podcasts
              </div>
              <div class="url">
                <code><xsl:value-of select="rss/channel/atom:link[@rel='first']/@href" /></code>
              </div>
            </div>
          </header>

          <main class="content">
            <div class="blurred-banner">
              <div class="blurcontainer">
                <div class="blurbackground" style="background-image: url('{rss/channel/itunes:image/@href}');"></div>
              </div>
              <div class="details-content">
                <img class="cover" src="{rss/channel/itunes:image/@href}">
                  <xsl:attribute name="srcset"><xsl:value-of select="rss/channel/podcast:images/@srcset" /></xsl:attribute>
                </img>
                <div class="details-text">
                  <h3><xsl:value-of select="rss/channel/title" /></h3>
                  <span><xsl:value-of select="rss/channel/description" /></span>
                </div>
              </div>
            </div>

            <nav>
              <div class="btn-container nav-prev">
                <xsl:if test="rss/channel/atom:link[@rel='previous']">
                  <a class="btn" href="{rss/channel/atom:link[@rel='previous']/@href}">&lt; Précédent</a>
                </xsl:if>
                &#160;
              </div>
              <div class="btn-container nav-next">
                &#160;
                <xsl:if test="rss/channel/atom:link[@rel='next']">
                  <a class="btn" href="{rss/channel/atom:link[@rel='next']/@href}">Suivant &gt;</a>
                </xsl:if>
              </div>
              <div class="subscribe"></div>
            </nav>

            <div class="all-eps">
              <xsl:for-each select="rss/channel/item">
                <div class="episode">
                  <a href="{link}" rel="bookmark">
                    <img class="cover" loading="lazy" alt="{title}" src="{itunes:image/@href}">
                      <xsl:attribute name="srcset"><xsl:value-of select="podcast:images/@srcset" /></xsl:attribute>
                    </img>
                  </a>
                  <div class="text">
                    <a href="{link}" rel="bookmark">
                      <h4><xsl:value-of select="title" /></h4>
                      <span>Présenté par <xsl:value-of select="itunes:author" /></span>
                    </a>
                    <br />
                    <blockquote><xsl:value-of select="description" /></blockquote>
                    <span class="details">
                      <xsl:if test="itunes:season and itunes:episode">
                        <span>Saison <xsl:value-of select="itunes:season" /> - Episode <xsl:value-of select="itunes:episode" /> - </span>
                      </xsl:if>
                      <span>Durée : <xsl:value-of select="itunes:duration" /></span>
                      <br />
                      <span>
                        Date de publication :
                        <time>
                          <xsl:attribute name="datetime"><xsl:value-of select="pubDate" /></xsl:attribute>
                          <xsl:value-of select="pubDate" />
                        </time>
                      </span>
                    </span>
                    <br />
                    <audio preload="metadata" controls="true" src="{enclosure[@type='audio/mpeg']/@url}"></audio>
                  </div>
                </div>
              </xsl:for-each>
            </div>

            <nav>
              <div class="btn-container nav-prev">
                <xsl:if test="rss/channel/atom:link[@rel='previous']">
                  <a class="btn" href="{rss/channel/atom:link[@rel='previous']/@href}">&lt; Précédent</a>
                </xsl:if>
                &#160;
              </div>
              <div class="btn-container nav-next">
                &#160;
                <xsl:if test="rss/channel/atom:link[@rel='next']">
                  <a class="btn" href="{rss/channel/atom:link[@rel='next']/@href}">Suivant &gt;</a>
                </xsl:if>
              </div>
            </nav>
          </main>

          <footer class="main-footer">
            <hr /><br />
            &#169; Radio Brony<br />
            <a href="https://radiobrony.fr/faq">FAQ</a>
            -
            <a href="https://radiobrony.fr/equipe">Équipe</a>
            -
            <a href="https://radiobrony.fr/privacy">Vie Privée</a>
          </footer>
        </div>

        <script>
          window.podcastData = {
            title: '<xsl:value-of select="rss/channel/title" />',
            subtitle: 'Présenté par <xsl:value-of select="rss/channel/itunes:author" />',
            description: null,
            cover: '<xsl:value-of select="rss/channel/itunes:image/@href" />',
            feeds: [
              {
                type: 'audio',
                format: 'mp3',
                url: '<xsl:value-of select="rss/channel/atom:link[@rel='first']/@href" />'
              }
            ]
          }

          const subBtn = document.createElement('script')
          subBtn.setAttribute('src', 'https://cdn.podlove.org/subscribe-button/javascripts/app.js')
          subBtn.setAttribute('data-color', '#469cd1')
          subBtn.setAttribute('data-json-data', 'podcastData')
          subBtn.setAttribute('data-language', 'fr')
          subBtn.setAttribute('data-size', 'small')
          subBtn.setAttribute('class', 'podlove-subscribe-button')
          setTimeout(_ => {
            document.querySelector('.subscribe').innerHTML = ''
            document.querySelector('.subscribe').appendChild(subBtn)
            setTimeout(_ => {
              document.querySelector('.subscribe').style.opacity = '100'
            }, 1000)
          }, 100)

          document.querySelectorAll('time').forEach(e => {
            const dt = luxon.DateTime.fromRFC2822(e.getAttribute('datetime'))
            e.innerHTML = dt.setLocale('fr').toLocaleString(luxon.DateTime.DATETIME_MED)
          })
        </script>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
