<?php
if (file_exists(__DIR__ . '/' . $_SERVER['REQUEST_URI'])) {
   return false; // serve the requested resource as-is.
}

include __DIR__ . '/index.php';

// php -S localhost:8888 routing.php
